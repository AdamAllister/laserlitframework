//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "c_app.h"

#include <stdio.h>

#include "SDL_image.h"
#include "SDL_ttf.h"

//  Initialises SDL, SDL Image, SDL Mixer and SDL TTF
//  Returns false when any fail to load and prints errors to command line
bool CApp::OnInit() {
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    printf("Could not initialise: %s\n", SDL_GetError());
    return false;
  }

  if ((sdl_window_ = SDL_CreateWindow("Laser Lit Engine", SDL_WINDOWPOS_UNDEFINED,
                                      SDL_WINDOWPOS_UNDEFINED, 640, 480, 0)) == NULL) {
    printf("Could not create window: %s\n", SDL_GetError());
    return false;
  }
  if ((sdl_renderer_ = SDL_CreateRenderer(sdl_window_, -1,
                                          SDL_RENDERER_ACCELERATED)) == NULL) {
    printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
    return false;
  } else {
    //Initialize renderer color
    SDL_SetRenderDrawColor(sdl_renderer_, 0x00, 0x00, 0x00, 0x00);

    //Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
      printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
      return false;
    }

    //Initialize SDL_ttf
    if (TTF_Init() == -1) {
      printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
      return false;
    }
  }


  return true;
}
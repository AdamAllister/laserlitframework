//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#ifndef LASETLIT_LL_RESOURCE_MANAGER_
#define LASETLIT_LL_RESOURCE_MANAGER_

#include <map>
#include <list>

#include "ll_resource.h"

//Manages all resources used by the game
class LLResourceManager {
 public:
  LLResourceManager();
  ~LLResourceManager();

  LLResource * FindResourceByID(Uint32 ID);
  //Unload all resources
  void Clear();
  //Create the resource list from an xml file
  bool LoadFromXMLFile(std::string filename);
  //Set the current game scene
  void SetCurrentScene(Uint32 scene);
  //Returns the total number of loaded and not unloaded resources
  const Uint32 get_resource_count() const {
    return resource_count_;
  }
 protected:
  //the current scene the game is set to
  Uint32 current_scene_;
  //Total number of resources unloaded and loaded
  Uint32 resource_count_;
  //Map of resources per scene <scene_, resource list>
  std::map<Uint32, std::list<LLResource*> > resources_;

 private:
};

#endif // LASETLIT_LL_RESOURCE_MANAGER_
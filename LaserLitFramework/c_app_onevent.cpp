//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "c_app.h"

//  Recieves an SDL_Event and ends the main loop if it is a quit event
void CApp::OnEvent(SDL_Event* sdl_event) {
  if (sdl_event->type == SDL_QUIT) {
    running_ = false;
  }
}
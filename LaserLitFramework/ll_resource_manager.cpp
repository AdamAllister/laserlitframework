//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "ll_resource_manager.h"

LLResourceManager::LLResourceManager() : current_scene_(0), resource_count_(0) {

}

//  Deletes all resource objects and nulls their pointers, and clears the
//  entire object.
LLResourceManager::~LLResourceManager() {

  for (std::map<Uint32, std::list<LLResource*>>::iterator resources_iter =
         resources_.begin(); resources_iter != resources_.end(); ++resources_iter) {
    std::list<LLResource*> list = resources_iter->second;
    for (std::list<LLResource*>::iterator list_iter = list.begin();
         list_iter != list.end(); ++list_iter) {
      if (*list_iter != nullptr) {
        delete *list_iter;
      }
    }
    list.clear();
  }
  resources_.clear();
  resource_count_ = 0;
  current_scene_ = 0;
}

//  Searches in the list for the current scene for a resource with the ID
//  Returns the resource if found or a null pointer
LLResource * LLResourceManager::FindResourceByID(Uint32 ID) {
  std::list<LLResource*> list = resources_[current_scene_];
  if (!list.empty()) {
    for (std::list<LLResource*>::iterator list_iter = list.begin();
         list_iter != list.end(); ++list_iter) {
      if ((*list_iter)->GetResourceID() == ID) {
        return *list_iter;
      }
    }
  }

  return nullptr;
}

//  TODO Add code that unloads all resource objects from all scenes
void LLResourceManager::Clear() {

}

//  TODO process xml file and create setup resource lists with it
bool LLResourceManager::LoadFromXMLFile(std::string filename) {
  return false;
}

//  Sets the active scene
void LLResourceManager::SetCurrentScene(Uint32 scene) {
  current_scene_ = scene;
}

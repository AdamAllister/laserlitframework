//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "ll_texture.h"

#include "SDL_image.h"

LLTexture::LLTexture() {
  texture_ = NULL;
  width_ = 0;
  height_ = 0;
}

LLTexture::~LLTexture() {
  Free();
}

//  Free texture if it exists
void LLTexture::Free() {
  if (texture_ != NULL) {
    SDL_DestroyTexture(texture_);
    texture_ = NULL;
    width_ = 0;
    height_ = 0;
  }
}

//  Modulate texture rgb
void LLTexture::set_color(Uint8 red, Uint8 green, Uint8 blue) {
  SDL_SetTextureColorMod(texture_, red, green, blue);
}

//  Set blending function
void LLTexture::set_blend_mode(SDL_BlendMode blending) {
  SDL_SetTextureBlendMode(texture_, blending);
}

//  Modulate texture alpha
void LLTexture::set_alpha(Uint8 alpha) {
  SDL_SetTextureAlphaMod(texture_, alpha);
}

//  Set rendering space and render to screen
void LLTexture::Render(SDL_Renderer* renderer, int x, int y, SDL_Rect* clip,
                       double angle, SDL_Point* center, SDL_RendererFlip flip) {

  SDL_Rect renderQuad = { x, y, width_, height_ };

  if (clip != NULL) {
    renderQuad.w = clip->w;
    renderQuad.h = clip->h;
  }

  SDL_RenderCopyEx(renderer, texture_, clip, &renderQuad, angle, center, flip);
}

int LLTexture::get_width() {
  return width_;
}

int LLTexture::get_height() {
  return height_;
}
//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#ifndef LASERLIT_LLTIMER_H_
#define LASERLIT_LLTIMER_H_

#include <SDL.h>

//Timer that uses SDLs ticks since initialization to keep track of time
class LLTimer {
 private:
  //The number of ticks when it started
  Uint32 ticks_at_start_;
  //The number of ticks when it paused
  Uint32 ticks_at_pause_;

  bool paused_;
  bool started_;

 public:
  LLTimer();

  //Timer Controls
  void Start();
  void Stop();
  void Pause();
  void UnPause();
  //Resets the timer and starts it again returning ticks before reset
  int Restart();

  //Gets the timer's time
  Uint32 GetTicks() const;

  //Checks the status of the timer
  bool is_started() const;
  bool is_paused() const;


};

#endif // LASERLIT_LLTIMER_H_
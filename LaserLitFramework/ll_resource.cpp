//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "ll_resource.h"

//Creates empty resource wrapper
LLResource::LLResource() : resource_id_(0), scene_(0), type_(RESOURCE_NULL) {}

LLResource::~LLResource() {}

void LLResource::SetResourceID(Uint32 ID) {
  resource_id_ = ID;
}
Uint32 LLResource::GetResourceID() const {
  return resource_id_;
}

void LLResource::SetFilename(std::string filename) {
  filename_ = filename;
}
std::string LLResource::GetFilename() const {
  return filename_;
}

void LLResource::SetResourceType(kResourceType type) {
  type_ = type;
}
kResourceType LLResource::GetResourceType() const {
  return type_;
}

void LLResource::SetResourceScope(Uint32 scene) {
  scene_ = scene;
}
Uint32 LLResource::GetResourceScope() const {
  return scene_;
}

bool LLResource::IsLoaded() const {
  return loaded_;
}
void LLResource::SetLoaded(bool value) {
  loaded_ = value;
}
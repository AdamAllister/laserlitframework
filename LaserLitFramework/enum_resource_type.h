//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

//Used to identify what kind of resource a resource is.
typedef enum {
  RESOURCE_NULL = 0,
  RESOURCE_GRAPHIC = 1,
  RESOURCE_MOVIE = 2,
  RESOURCE_AUDIO = 3,
  RESOURCE_TEXT = 4,
} kResourceType;
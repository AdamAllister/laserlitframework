//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#ifndef LASETLIT_LL_RESOURCE_
#define LASETLIT_LL_RESOURCE_

#include <string>

#include "SDL.h"

#include "enum_resource_type.h"

//An abstract resource wrapper for all resources (graphics, video, sound, etc)
//Used by resource manager to store resources together
class LLResource {
 public:
  LLResource();
  virtual ~LLResource();
  //Implemented by child class to load whatever resource it is
  virtual void Load() = 0;
  //Implemented by child class to unloaded whatever resource it is
  virtual void Unload() = 0;

  //Get and set the resource identifier
  void SetResourceID(Uint32 ID);
  Uint32 GetResourceID() const;

  //Get and set the resources filename (E.g. "blah.png")
  void SetFilename(std::string filename);
  std::string GetFilename() const;

  //Get and set the resource type (E.g. Grahpics, Video, Audio,etc)
  void SetResourceType(kResourceType type);
  kResourceType GetResourceType() const;

  //Get and set the scene the resource is used in
  void SetResourceScope(Uint32 scene);
  Uint32 GetResourceScope() const;

  //Get and set if the resource is loaded
  bool IsLoaded() const;
  void SetLoaded(bool value);

 protected:
  //Used to identify a resource
  Uint32 resource_id_;
  //Scene the resource is used in
  Uint32 scene_;
  //name of the file
  std::string filename_;
  //Type of resource
  kResourceType type_;
  //Is it loaded
  bool loaded_;
};

#endif // LASETLIT_LL_RESOURCE_
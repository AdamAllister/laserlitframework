//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "c_app.h"

#include <stdio.h>
#include <sstream>

CApp::CApp() {
  sdl_window_ = NULL;
  sdl_renderer_ = NULL;

  running_ = true;
}

//  Main loop
int CApp::OnExecute() {
  if (OnInit() == false) {
    return -1;
  }

  SDL_Event sdl_event;

  //The actual frames per second timer
  LLTimer fpsTimer;

  //The timer used to cap the frame rate
  LLTimer capTimer;

  //Start counting frames per second
  int countedFrames = 0;
  fpsTimer.Start();

  while (running_) {
    //Start cap timer
    capTimer.Start();

    while (SDL_PollEvent(&sdl_event)) {
      OnEvent(&sdl_event);
    }

    //Calculate and correct fps
    float avgFPS = countedFrames / (fpsTimer.GetTicks() / 1000.f);
    if (avgFPS > 2000000) {
      avgFPS = 0;
    }

    printf("Average Frames Per Second (With Cap) %f\n", avgFPS);


    OnLoop();
    OnRender();

    ++countedFrames;

    //If frame finished early
    int frameTicks = capTimer.GetTicks();
    if (frameTicks < KTicksPerFrame) {
      //Wait remaining time
      SDL_Delay(KTicksPerFrame - frameTicks);
    }
  }

  OnCleanup();

  return 0;
}

//  Entry Point
int main(int argc, char* argv[]) {
  CApp theApp;

  return theApp.OnExecute();

}
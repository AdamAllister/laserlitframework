//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#ifndef LASERLIT_LLTEXTURE_H
#define LASERLIT_LLTEXTURE_H

#include <string>

#include <SDL.h>
#include <SDL_ttf.h>

//  A texture wrapper for SDL_Texture.
//  Stores texture dimensions and provides shader functions
class LLTexture {
 public:
  //Initializes variables
  LLTexture();

  //Deallocates memory
  ~LLTexture();

  //Loads image at specified path
  bool LoadFromFile(SDL_Renderer* renderer, std::string path);

#ifdef _SDL_TTF_H
  //Creates image from font string
  bool LoadFromRenderedText(SDL_Renderer* renderer, TTF_Font* font,
                            std::string texture_text, SDL_Color text_color);
#endif

  //Deallocates texture
  void Free();

  //Set color modulation
  void set_color(Uint8 red, Uint8 green, Uint8 blue);

  //Set blending
  void set_blend_mode(SDL_BlendMode blending);

  //Set alpha modulation
  void set_alpha(Uint8 alpha);

  //Renders texture at given point
  void Render(SDL_Renderer* renderer, int x, int y, SDL_Rect* clip = NULL,
              double angle = 0.0, SDL_Point* center = NULL,
              SDL_RendererFlip flip = SDL_FLIP_NONE);

  //Gets image dimensions
  int get_width();
  int get_height();

 private:
  //The actual hardware texture
  SDL_Texture* texture_;

  //Image dimensions
  int width_;
  int height_;
};

#endif // LASERLIT_LLTEXTURE_H
//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "ll_timer.h"

LLTimer::LLTimer() {
  ticks_at_start_ = 0;
  ticks_at_pause_ = 0;

  paused_ = false;
  started_ = false;
}

//  Starts the timer, setting the start time at the current tick
void LLTimer::Start() {
  started_ = true;
  paused_ = false;

  //Get the current clock time
  ticks_at_start_ = SDL_GetTicks();
  ticks_at_pause_ = 0;
}

//  Stops the timer, removing all tick values
void LLTimer::Stop() {
  //Stop the timer
  started_ = false;

  //Unpause the timer
  paused_ = false;

  //Clear tick variables
  ticks_at_start_ = 0;
  ticks_at_pause_ = 0;
}

//  Pauses the timer, storing the amount of ticks that have passed
void LLTimer::Pause() {
  //If the timer is running and isn't already paused
  if (started_ && !paused_) {
    //Pause the timer
    paused_ = true;

    //Calculate the paused ticks
    ticks_at_pause_ = SDL_GetTicks() - ticks_at_start_;
    ticks_at_start_ = 0;
  }
}

//  Unpauses the timer, setting the start time to the current amount of ticks
//  minus the amount of time that passed on the timer.
//  (E.g. if the current ticks is 10000 and the amount of ticks the timer timed
//  was 1000 then the start time would now be 9000)
void LLTimer::UnPause() {
  //If the timer is running and paused
  if (started_ && paused_) {
    //Unpause the timer
    paused_ = false;

    //Reset the starting ticks
    ticks_at_start_ = SDL_GetTicks() - ticks_at_pause_;

    //Reset the paused ticks
    ticks_at_pause_ = 0;
  }
}

//  Returns the current ticks passed on the timer if started or the ticks passed
//  at paused if paused, or zero if it is not running.
Uint32 LLTimer::GetTicks() const {
  //The actual timer time
  Uint32 time = 0;

  if (started_) {
    if (paused_) {
      //Return the number of ticks when the timer was paused
      time = ticks_at_pause_;
    } else {
      //Return the current time minus the start time
      time = SDL_GetTicks() - ticks_at_start_;
    }
  }
  return time;
}

//  Returns true if timer is running and paused or unpaused
bool LLTimer::is_started() const {

  return started_;
}

//  Returns true if timer is running and paused
bool LLTimer::is_paused() const {
  return paused_ && started_;
}
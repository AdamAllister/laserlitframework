//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "c_app.h"

//  Updates anything after processing events and before rendering
void CApp::OnLoop() {

}
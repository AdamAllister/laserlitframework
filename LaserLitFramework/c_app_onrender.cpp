//  The Laser Lit Framework is a Game Development Framework written in C++
//  Copyright(C) 2016  Adam Allister
//
//  This file is part of the Laser Lit Framework.

#include "c_app.h"

#include "ll_texture.h"

//  Clears and draws things to renderer, then displays it on screen.
void CApp::OnRender() {
  //Clear screen
  SDL_RenderClear(sdl_renderer_);

  // Render Things here


  //Update screen
  SDL_RenderPresent(sdl_renderer_);
}